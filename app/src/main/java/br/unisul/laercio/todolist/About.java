package br.unisul.laercio.todolist;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;

public class About {

    public void showAboutAlert(View view, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.drawable.ic_about_icon);
        builder.setTitle(R.string.app_name);
        builder.setView(view);
        builder.create();
        builder.show();
    }
}
