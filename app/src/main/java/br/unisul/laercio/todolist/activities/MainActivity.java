package br.unisul.laercio.todolist.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;

import br.unisul.laercio.todolist.About;
import br.unisul.laercio.todolist.db.DatabaseHelper;
import br.unisul.laercio.todolist.R;

public class MainActivity extends AppCompatActivity {

    private int itemID = -1;
    DatabaseHelper dbHelper;
    ListView listViewTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listViewTasks = findViewById(R.id.lstTask);
        listViewTasks.setEnabled(true);
        dbHelper = new DatabaseHelper(getApplicationContext());

        showTodolist();

        final FloatingActionButton addFab = findViewById(R.id.fab);
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addButtonClicked();
            }
        });

        listViewTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String taskName = parent.getItemAtPosition(position).toString();
                Cursor cursor = dbHelper.getItemID(taskName);
                itemID = -1;

                while (cursor.moveToNext()) {
                    itemID = cursor.getInt(0);
                }

                dbHelper.updateFlag(itemID);
                showTodolist();
            }
        });

        listViewTasks.setLongClickable(true);
        listViewTasks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showMenuPopup(view, parent, position);
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main_menu_taskist, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void showMenuPopup(View v, final AdapterView<?> parent, final int position) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.inflate(R.menu.listview_item_menu);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.op_editar:
                        final String oldTask = parent.getItemAtPosition(position).toString();
                        Cursor cursor = dbHelper.getItemID(oldTask);
                        itemID = -1;

                        while (cursor.moveToNext()) {
                            itemID = cursor.getInt(0);
                        }

                        final EditText text = new EditText(MainActivity.this);
                        text.setText(oldTask);

                        final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                        alert.setTitle("Alterar tarefa");
                        alert.setView(text);
                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String newTask = text.getText().toString();
                                dbHelper.updateTask(oldTask, newTask, itemID);
                                showTodolist();
                            }
                        });
                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                        return true;

                    // Se a opção "Remover" for selecionada, pega o ID da task e a remove do banco.
                    case R.id.op_remover:
                        final String taskName = parent.getItemAtPosition(position).toString();
                        Cursor rCursor = dbHelper.getItemID(taskName);
                        itemID = -1;

                        while (rCursor.moveToNext()) {
                            itemID = rCursor.getInt(0);
                        }

                        dbHelper.deleteTask(itemID, taskName);
                        toastMessage("Task removida com sucesso");
                        showTodolist();
                        return true;
                }
                return false;
            }
        });
        popup.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Função ao "clicar" s obre a opção "To Do Tasks".
            case R.id.taskist:
                toastMessage("Você já está na To Do Tasks");
                return true;

            // Função ao "clicar" sobre a opção "Done Tasks".
            case R.id.doneist:
                Intent intent = new Intent(this, DoneActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                return true;

            // Função ao "clicar" sobre a opção "Delete List".
            case R.id.deleteTodoList:
                if (checkIfTableIsEmpty()) {
                    dbHelper.deleteAllTodoTasks();
                    toastMessage("Lista removida com sucesso");
                    showTodolist();
                } else toastMessage("Não há tasks na to do list");
                return true;

            // Função ao "clicar" sobre a opção "About".
            case R.id.about:
                About about = new About();
                View messageView = getLayoutInflater().inflate(R.layout.activity_about,null, false);
                about.showAboutAlert(messageView, MainActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Verifica se a tabela está vazia.
    private boolean checkIfTableIsEmpty() {
        Cursor cursor = dbHelper.getTaskList();
        if (cursor.moveToNext()) return true;
        else return false;
    }

    private void addButtonClicked() {
        final EditText itemEditText = new EditText(this);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Nova Tarefa")
                .setView(itemEditText)
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String item = String.valueOf(itemEditText.getText());
                        if (item.isEmpty() || item.equals("")) {
                            toastMessage("Nenhuma tarefa informada");
                        } else {
                            String task = String.valueOf(itemEditText.getText());
                            dbHelper.insertNewTask(task, 0);
                            showTodolist();

                            toastMessage("Tarefa adicionada com sucesso");
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        toastMessage("Operação cancelada");
                    }
                })
                .create();
        dialog.show();
    }

    private void showTodolist() {
        Cursor cursor = dbHelper.getTaskList();
        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(1));
        }
        ListAdapter doneAdapter = new ArrayAdapter<>(this, R.layout.row_todo_task, R.id.taskName, list);
        listViewTasks.setAdapter(doneAdapter);
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
