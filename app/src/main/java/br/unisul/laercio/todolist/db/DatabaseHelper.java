package br.unisul.laercio.todolist.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "TodoListDB";
    private static final int DB_VERSION = 1;
    public static final String DB_TABLE = "Task";

    private static final String COLUMN_ID = "ID";
    public static final String COLUMN_NAME = "TaskName";
    public static final String COLUMN_FLAG = "FLAG";

    public static final int FLAG_TASK_TODO = 0;
    public static final int FLAG_TASK_DONE = 1;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (ID INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL, FLAG INTEGER)", DB_TABLE, COLUMN_NAME);
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DROP TABLE IF EXISTS %s", DB_TABLE);
        db.execSQL(query);
        onCreate(db);
    }

    public Cursor getItemID(String taskName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_ID + " FROM " + DB_TABLE + " WHERE " + COLUMN_NAME + " = '" + taskName + "' AND " + COLUMN_FLAG + " = " + FLAG_TASK_TODO;
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }

    public void insertNewTask(String task, int flag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(COLUMN_NAME, task);
        content.put(COLUMN_FLAG, flag);
        db.insertWithOnConflict(DB_TABLE, null, content, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public void updateFlag(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + DB_TABLE + " SET " + COLUMN_FLAG + " = " + FLAG_TASK_DONE + " WHERE " + COLUMN_ID + " = " + id;
        db.execSQL(query);
    }

    public void updateTask(String oldTask, String newTask, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + DB_TABLE + " SET " + COLUMN_NAME + " = '" +  newTask + "' " +
                "WHERE " + COLUMN_ID + " = " + id + " AND " + COLUMN_NAME + " = '" + oldTask + "'";
        db.execSQL(query);
    }

    // Query para deletar toda a to do list.
    public void deleteAllTodoTasks() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + DB_TABLE + " WHERE " + COLUMN_FLAG + " = " + FLAG_TASK_TODO;
        db.execSQL(query);
    }

    // Query para deletar toda a done list.
    public void deleteAllDoneTasks() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + DB_TABLE + " WHERE " + COLUMN_FLAG + " = " + FLAG_TASK_DONE;
        db.execSQL(query);
    }

    // Query para deletar uma dupla específica no banco.
    public void deleteTask(int id, String taskName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + DB_TABLE + " WHERE " + COLUMN_ID + " = " + id +
                " AND " + COLUMN_NAME + " = '" + taskName + "'";
        db.execSQL(query);
    }

    // Query para selecionar todas as tuplas da tabela com a flag 0.
    public Cursor getTaskList() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + DB_TABLE + " WHERE " + COLUMN_FLAG + " = " + FLAG_TASK_TODO;
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }

    public Cursor getDoneTasks() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + DB_TABLE + " WHERE " + COLUMN_FLAG + " = " + FLAG_TASK_DONE;
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }
}
