package br.unisul.laercio.todolist.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.unisul.laercio.todolist.About;
import br.unisul.laercio.todolist.db.DatabaseHelper;
import br.unisul.laercio.todolist.R;

public class DoneActivity extends AppCompatActivity {

    private ListView doneListView;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        doneListView = (ListView) findViewById(R.id.listDone);
        dbHelper = new DatabaseHelper(getApplicationContext());

        populateDoneTaskList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_doneist, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Função ao "clicar" sobre a opção "To Do Tasks".
            case R.id.goToTodoList:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                return true;

            // Função ao "clicar" sobre a opção "Done Tasks".
            case R.id.goToDoneList:
                toastMessage("Você já está na Done Tasks");
                return true;

            // Função ao "clicar" sobre a opção "Delete Done List".
            case R.id.deleteDoneList:
                if (checkIfTableIsEmpty()) {
                    dbHelper.deleteAllDoneTasks();
                    toastMessage("Lista removida com sucesso");
                    populateDoneTaskList();
                } else toastMessage("Não há tasks na done list");
                return true;

            // Função ao "clicar" sobre a opção "About".
            case R.id.about:
                About about = new About();
                View messageView = getLayoutInflater().inflate(R.layout.activity_about,null, false);
                about.showAboutAlert(messageView, DoneActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void populateDoneTaskList() {
        Cursor cursor = dbHelper.getDoneTasks();
        ArrayList<String> list = new ArrayList<>();
        while(cursor.moveToNext()) {
            list.add(cursor.getString(1));
        }
        ListAdapter doneAdapter = new ArrayAdapter<>(this, R.layout.row_done_task, R.id.doneTaskName, list);
        doneListView.setAdapter(doneAdapter);
    }

    // Verifica se a tabela está vazia.
    private boolean checkIfTableIsEmpty() {
        Cursor cursor = dbHelper.getDoneTasks();
        if (cursor.moveToNext()) return true;
        else return false;
    }

    // Método auxiliar para exibir mensagens na DoneActivity.
    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
